# Service Provider Routing and Switching, Specialist (JNCIS-SP) JN0-361
[Blueprint on juniper.net](https://www.juniper.net/us/en/training/certification/certification-tracks/sp-routing-switching-track?tab=jncis-sp)

## Protocol-Independent Routing
Identify the concepts, operation and functionality of various protocol-independent routing components

### Static, aggregate, and generated routes
- Martian addresses
- Routing instances, including RIB groups
- Load balancing
- Filter-based forwarding

### Demonstrate knowledge of how to configure and monitor various protocol-independent routing components
- Static, aggregate, and generated routes
- Load balancing
- Filter-based forwarding

## Open Shortest Path First (OSPF)
### Identify the concepts, operation, or functionality of OSPF
- Link-state database
- OSPF packet types
- Router ID
- Adjacencies and neighbors
- Designated router (DR) and backup designated router (BDR)
- OSPF area and router types
- LSA packet types

### Demonstrate knowledge of how to configure, monitor and troubleshoot IS-IS
- Areas, interfaces and neighbors
- Additional basic options
- Routing policy application
- Troubleshooting tools

## Intermediate System to Intermediate System (IS-IS)
### Identify the concepts, operation and functionality of IS-IS
- Link-state database
- IS-IS PDUs
- TLVs
- Adjacencies and neighbors
- Levels and areas
- Designated intermediate system (DIS)
- Metrics

### Demonstrate knowledge of how to configure, monitor and troubleshoot IS-IS
- Levels, interfaces, and adjacencies
- Additional basic options
- Routing policy application
- Troubleshooting tools

## Border Gateway Protocol (BGP)
### Identify the concepts, operation and functionality of BGP
- BGP basic operation
- BGP message types
- Attributes
- Route/path selection process
- IBGP and EBGP functionality and interaction

### Demonstrate knowledge of how to configure, monitor, or troubleshoot BGP
- Groups and peers
- Additional basic options
- Routing policy application

## Layer 2 Bridging and VLANs
### Identify the concepts, operation, or functionality of Layer 2 bridging for the Junos OS
- Service Provider switching platforms
- Bridging elements and terminology
- Frame processing
- Virtual Switches
- Provider bridging (e.g., Q-in-Q tunneling)

### Identify the concepts, benefits, and functionality of VLANs
- Port modes
- Tagging
- MVRP
- IRB

### Demonstrate knowledge of how to configure, monitor, or troubleshoot Layer 2 bridging and VLANs
- Interfaces and ports
- VLANs
- MVRP
- IRB
- Provider bridging

## Spanning-Tree Protocols
### Identify the concepts, benefits, operation, or functionality of Spanning Tree Protocol and its variants
- STP, RSTP, MSTP and VSTP concepts
- Port roles and states
- BPDUs
- Convergence and reconvergence
- Spanning-tree security

### Demonstrate knowledge of how to configure, monitor or troubleshoot STP and its variants
- Spanning-tree protocols: STP, RSTP, MSTP, VSTP
- BPDU, loop, and root protection

## Multiprotocol Label Switching (MPLS)
### Identify the concepts, operation, or functionality of MPLS
- MPLS terminology
- MPLS packet header
- End-to-end packet flow and forwarding
- Labels and the label information base (LIB)
- MPLS and routing tables
- RSVP
- LDP

### Demonstrate knowledge of how to configure, monitor, or troubleshoot MPLS
- MPLS forwarding
- RSVP-signaled and LDP-signaled LSPs

## IPv6
### Identify the concepts, operation, or functionality of IPv6
- IPv4 vs. IPv6
- Address types, notation and format
- Address scopes
- Autoconfiguration
- Tunneling

### Demonstrate knowledge of how to configure, monitor, or troubleshoot IPv6
- Interfaces
- Static routes
- Dynamic routing: OSPFv3, IS-IS, BGP
- IPv6 over IPv4 tunneling

## Tunnels
### Identify the concepts, requirements, or functionality of IP tunneling
- Tunneling applications and considerations
- GRE
- IP-IP

### Demonstrate knowledge of how to configure, monitor, or troubleshoot IP tunnels
- GRE
- IP-IP

## High Availability
### Identify the concepts, benefits, applications, or requirements of high availability
- Link aggregation groups (LAG) and multichassis LAGs (MC-LAGs)
- Graceful restart (GR)
- Graceful Routing Engine switchover (GRES)
- Nonstop active routing (NSR)
- Nonstsop bridging (NSB)
- Bidirectional Forwarding Detection (BFD)
- Virtual Router Redundancy Protocol (VRRP)
- Unified In-Service Software Upgrade (ISSU)
- Ethernet Ring Protection (ERP)

### Demonstrate knowledge of how to configure, monitor, or troubleshoot high availability components
- LAG, MC-LAG
- GR, GRES, NSR and NSB
- VRRP
- ISSU