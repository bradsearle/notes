# Implementing Cisco SD-WAN Solutions v1.0 (300-415)
[Blueprint on cisco.com](https://learningnetwork.cisco.com/community/certifications/ccnp-enterprise/ensdwi/exam-topics)
  
## 1 - Architecture
### 1.1 - Describe Cisco SD-WAN Architecture and Components
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/System_Overview/Components_of_the_Viptela_SEN/01Components_of_the_Viptela_Solution

https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/Plug_and_Play_Support_Guide_for_Cisco_SD-WAN_Products

### 1.1.a - Orchestration plane (vBond, NAT)
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/System_Overview/Components_of_the_Viptela_SEN/01Components_of_the_Viptela_Solution#vBond_Orchestrator

### 1.1.b - Management plane (vManage)
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/System_Overview/Components_of_the_Viptela_SEN/01Components_of_the_Viptela_Solution#vManage_NMS

### 1.1.c - Control plane (vSmart, OMP)
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/System_Overview/Components_of_the_Viptela_SEN/01Components_of_the_Viptela_Solution#vSmart_Controller

https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/System_Overview/Working_with_the_Viptela_SEN/02Viptela_Terminology#OMP_Routes

https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.4/Security/01Security_Overview/Control_Plane_Security_Overview

### 1.1.d - Data plane (vEdge)
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/System_Overview/Components_of_the_Viptela_SEN/01Components_of_the_Viptela_Solution#vEdge_Routers

https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.4/Security/01Security_Overview/Data_Plane_Security_Overview

### 1.1.d - (i) TLOC
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/System_Overview/Working_with_the_Viptela_SEN/02Viptela_Terminology#TLOC

https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.2/03Routing/01Unicast_Overlay_Routing_Overview#TLOC_Routes

### 1.1.d - (ii) IPsec
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.4/Security/02Configuring_Security_Parameters#Configure_Data_Plane_Security_Parameters

https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.4/Security/02Configuring_Security_Parameters/Configuring_IKE-Enabled_IPsec_Tunnels

### 1.1.d - (iii) vRoute
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.3/03Routing/01Unicast_Overlay_Routing_Overview#OMP_Route_Advertisements

://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.2/03Routing/01Unicast_Overlay_Routing_Overview#OMP_Routing_Protocol

### 1.1.d - (iv) BFD
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_16.3/09High_Availability_and_Scaling/02Configuring_Control_Pland_and_Data_Plane_High_Availability_Parameters#Data_Plane_High_Availability

### 1.2 - Describe WAN Edge platform types, capabilities (vEdges, cEdges)
https://www.cisco.com/c/en/us/solutions/collateral/enterprise-networks/sd-wan/nb-07-vedge-routers-data-sheet-cte-en.html

## 2 - Controller Deployment
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/Viptela_Overlay_Network_Bringup/01Bringup_Sequence_of_Events

### 2.1 - Describe controller cloud deployment

### 2.2 - Describe Controller on-Prem Deployment

### 2.2.a - Hosting platform (KVM/Hypervisor)

### 2.2.b - Installing controllers
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/Viptela_Overlay_Network_Bringup/03Deploy_the_vManage_NMS

https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/Viptela_Overlay_Network_Bringup/04Deploy_the_vBond_Orchestrator

https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/Viptela_Overlay_Network_Bringup/05Step_4%3A_Deploy_the_vContainer_Host

https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/Viptela_Overlay_Network_Bringup/06Deploy_the_vSmart_Controller

### 2.2.c - Scalability and redundancy

### 2.3 - Configure and verify certificates and whitelisting

### 2.4 - Troubleshoot control-plane connectivity between controllers

## 3 - Router Deployment
### 3.1 - Describe WAN Edge deployment
### 3.1.a - On-boarding
### 3.1.b - Orchestration with zero-touch provisioning/plug-and-play
https://sdwan-docs.cisco.com/Product_Documentation/Getting_Started/Plug_and_Play_Support_Guide_for_Cisco_SD-WAN_Products

### 3.1.c - Single/multi data center/regional hub deployments

### 3.2 - Configure and verify SD-WAN data plane

### 3.2.a - Circuit termination/TLOC-extension
https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/Configuration_Commands/tloc-extension

### 3.2.b - Underlay-overlay connectivity
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.1/03Routing/03Configuring_Unicast_Overlay_Routing

https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/Configuration_Commands/overlay-as


### 3.3 - Configure and verify OMP
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.2/03Routing/02Configuring_OMP

### 3.4 - Configure and verify TLOCs
https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/Configuration_Commands/tloc-extension

https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/Operational_Commands/show_omp_tlocs

### 3.5 - Configure and verify CLI and vManage feature configuration templates

### 3.5.a - VRRP
https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/Configuration_Commands/vrrp

### 3.5.b - OSPF
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.2/03Routing/03Configuring_Unicast_Overlay_Routing#Set_Up_Basic_OSPF_on_a_vEdge_Router

https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.2/03Routing/03Configuring_Unicast_Overlay_Routing#Configure_OSPF_Transport-Side_Routing

### 3.5.c - BGP
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.2/03Routing/03Configuring_Unicast_Overlay_Routing#Set_Up_Basic_BGP_on_a_vEdge_Router

https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.2/03Routing/03Configuring_Unicast_Overlay_Routing#Configure_BGP_Transport-Side_Routing

## 4 - Policies
https://sdwan-docs.cisco.com/Product_Documentation/Software_Features/Release_18.4/06Policy_Basics/01Policy_Overview/01Viptela_Policy_Framework_Basics

### 4.1 - Configure and verify control policies
### 4.2 - Configure and verify data policies
### 4.3 - Configure and verify end-to-end segmentation
### 4.3.a - VPN segmentation
### 4.3.b - Topologies
### 4.4 - Configure and verify SD-WAN application-aware routing
### 4.5 - Configure and verify direct Internet access

## 5 - Security and Quality of Service
https://www.cisco.com/c/en/us/td/docs/routers/sdwan/software/configuration/sdwan-xe-16-11-book/security.html

https://community.cisco.com/t5/networking-documents/sd-wan-configuration-example-site-to-site-lan-to-lan-ipsec/ta-p/3807905

### 5.1 - Configure and verify service insertion
### 5.2 - Describe application-aware firewall
### 5.3 - Configure and verify QoS treatment on WAN edge routers
### 5.3.a - Scheduling
### 5.3.b - Queuing
### 5.3.c - Shaping
### 5.3.d - Policing

## 6 - Management and Operations
### 6.1 - Describe monitoring and reporting from vManage
https://sdwan-docs.cisco.com/Product_Documentation/vManage_How-Tos/Troubleshooting/Monitor_Alarms

https://sdwan-docs.cisco.com/Product_Documentation/vManage_How-Tos/Troubleshooting/Monitor_Event_Notifications

### 6.2 - Configure and verify monitoring and reporting


### 6.3 - Describe REST API monitoring
https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/vManage_REST_APIs

https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/vManage_REST_APIs/vManage_REST_APIs_Overview/Using_the_vManage_REST_APIs

https://sdwan-docs.cisco.com/Product_Documentation/Command_Reference/vManage_REST_APIs/Real-Time_Monitoring_APIs

### 6.4 - Describe software upgrade from vManage

